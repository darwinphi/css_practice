(() => {
	const openModal = document.querySelector("[data-js=open-modal]")
	const closeModal = document.querySelector("[data-js=close-modal]")
	const overlay = document.querySelector("[data-js=overlay]")
	const modal = document.querySelector("[data-js=modal]")

	openModal.addEventListener("click", () => {
		overlay.classList.add("overlay--open")
		modal.classList.add("modal--open")
	})

	overlay.addEventListener("click", (e) => {
		if (e.target === overlay) {
			close()
		}
	})

	closeModal.addEventListener("click", () => {
		close()
	})

	function close() {
		modal.classList.remove("modal--open")
		modal.classList.add("modal--hide")

		setTimeout(() => {
			overlay.classList.remove('overlay--open')	
			modal.classList.remove('modal--hide')
		}, 400)
	}
})()