(() => {
	const openMessage = document.querySelector("[data-js=open-message]")
	const popUp = document.querySelector("[data-js=pop-up]")

	openMessage.addEventListener("click", () => {
		popUp.classList.add("pop-up--open")
		setTimeout(() => {
			popUp.classList.remove("pop-up--open")
		}, 2000)
	})
})()