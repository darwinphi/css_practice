(() => {
	const content = document.querySelector("[data-js=content]")
	const height = content.scrollHeight
	
	height.style.setProperty("--max-height", height + "px")
})()