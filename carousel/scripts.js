(() => {
	const container = document.querySelector("[data-js=container]")
	const one 			= document.querySelector("[data-js=one]")
	const two 			= document.querySelector("[data-js=two]")
	const three 		= document.querySelector("[data-js=three]")
	const four 			= document.querySelector("[data-js=four]")

	one.addEventListener("click", () => {
		container.classList.toggle("show-two")
	})

	two.addEventListener("click", () => {
		container.classList.toggle("show-three")
	})

	three.addEventListener("click", () => {
		container.classList.toggle("show-four")
	})

	four.addEventListener("click", () => {
		container.classList.remove("show-two")
		container.classList.remove("show-three")
		container.classList.remove("show-four")
	})

})()

